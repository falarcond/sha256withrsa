﻿using Newtonsoft.Json;
using System;
using System.Collections.Specialized;
using System.Net;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace SHA256withRSA
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                // certificate
                var certificate = new X509Certificate2(@"C:\midyear-task-242420-0a6a081a6505.p12", "notasecret", X509KeyStorageFlags.Exportable);

                // header
                var header = new { alg = "RS256", typ = "JWT" };

                // claimset
                var times = GetExpiryAndIssueDate();
                var claimset = new
                {
                    iss = args[0],
                    scope = "https://www.googleapis.com/auth/calendar",
                    aud = "https://accounts.google.com/o/oauth2/token",
                    exp = times[1],
                    iat = times[0],
                };

                // encoded header
                var headerSerialized = JsonConvert.SerializeObject(header);
                var headerBytes = Encoding.UTF8.GetBytes(headerSerialized);
                var headerEncoded = Convert.ToBase64String(headerBytes);

                // encoded claimset
                var claimsetSerialized = JsonConvert.SerializeObject(claimset);
                var claimsetBytes = Encoding.UTF8.GetBytes(claimsetSerialized);
                var claimsetEncoded = Convert.ToBase64String(claimsetBytes);

                // input
                var input = headerEncoded + "." + claimsetEncoded;
                var inputBytes = Encoding.UTF8.GetBytes(input);

                // signiture
                byte[] signatureBytes = SignData(certificate, inputBytes);
                var signatureEncoded = Convert.ToBase64String(signatureBytes);

                // jwt
                var jwt = headerEncoded + "." + claimsetEncoded + "." + signatureEncoded;
                Console.Write(jwt);
            }
            catch (CryptographicException e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private static int[] GetExpiryAndIssueDate()
        {
            var utc0 = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            var issueTime = DateTime.UtcNow;

            var iat = (int)issueTime.Subtract(utc0).TotalSeconds;
            var exp = (int)issueTime.AddMinutes(55).Subtract(utc0).TotalSeconds;

            return new[] { iat, exp };
        }

        private static byte[] SignData(X509Certificate2 certificate, byte[] dataToSign)
        {
            // get xml params from current private key
            var rsa = (RSA)certificate.PrivateKey;
            var xml = RSAHelper.ToXmlString(rsa, true);
            var parameters = RSAHelper.GetParametersFromXmlString(rsa, xml);

            // generate new private key in correct format
            var cspParams = new CspParameters()
            {
                ProviderType = 24,
                ProviderName = "Microsoft Enhanced RSA and AES Cryptographic Provider"
            };
            var rsaCryptoServiceProvider = new RSACryptoServiceProvider(cspParams);
            rsaCryptoServiceProvider.ImportParameters(parameters);

            // sign data
            var signedBytes = rsaCryptoServiceProvider.SignData(dataToSign, HashAlgorithmName.SHA256, RSASignaturePadding.Pkcs1);

            return signedBytes;
        }
    }
}
